#!/usr/bin/python

import os
import pandas as pd
import jaydebeapi
from sqlalchemy import types
import time
from py_scripts import clients,terminals,transactions,pssp_black,clients,accounts,cards,fraud
conn = jaydebeapi.connect(
"oracle.jdbc.driver.OracleDriver",
"jdbc:oracle:---------",
["----","----k"],
"/home/demipt2/ojdbc8.jar"
)
conn.jconn.setAutoCommit(False)
curs = conn.cursor()

# empty folder for etl-passed files
if not os.path.isdir("archive"):
     os.mkdir("archive")

LOAD_FILES = os.listdir('./data')  # it is a constant, variable name begins with capital letter
LOAD_FILES.sort()  # sort ascending to capture the 1st element for uploading
load_date = " "

if len(LOAD_FILES) == 0:
    print('\033[31mFiles not found\033[0m')
if not ('transactions_' + load_date + '.txt') in os.listdir('./data'):
    print('\033[31mFile transactions not found\033[0m')
if not ('passport_blacklist_' + load_date + '.xlsx') in os.listdir('./data'):
    print('\033[31mFile passport_blacklist not found\033[0m')
if not ('terminals_' + load_date + '.xlsx') in os.listdir('./data'):
    print('\033[31mFile terminals not found\033[0m')

conn.commit()
curs.close()
conn.close()
